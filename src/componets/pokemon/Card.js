import React, {useState} from "react";
import axios from "axios";

const PokemonCard = ({nombre, url, pokeNumber}) => {
  const [pokeInfo, setPokeInfo] = useState(undefined);
  const getPokeInfo = async () => {
    if (pokeInfo === undefined) {
      const response = await axios.get(url);
      setPokeInfo(response.data);
    }
  };

  return (
    <div className="col s12 m6 l4">
      <div className="card" onClick={getPokeInfo}>
        <div className="card-image waves-effect waves-block waves-light">
          <img
              className="activator"
              src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokeNumber}.png`}
              alt=""
          />
        </div>
        <div className="card-content">
          <span className="card-title activator grey-text text-darken-4 capitalize">
            {nombre}
            <i className="material-icons right">more_vert</i>
          </span>
        </div>
        <div className="card-reveal">
          <span className="card-title grey-text text-darken-4 capitalize">
            {nombre}
            <i className="material-icons right">close</i>
          </span>
          {pokeInfo !== undefined && (
              <blockquote>
                <div className="chip">Types</div>
                {pokeInfo.types.map(({type}) => (
                    <p className="capitalize">{type.name}</p>
                ))}
              </blockquote>
          )}
        </div>
      </div>
    </div>
  );
};

export default PokemonCard;
