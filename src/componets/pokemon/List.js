import React, {Component} from "react";
import PokemonCard from "./Card";
import axios from "axios";

class PokemonList extends Component {
  state = {
    list: []
  };

  componentDidMount() {
    axios
        .get("https://pokeapi.co/api/v2/pokemon?limit=151")
        .then(({data}) => this.setState({list: data.results}))
        .catch(err => console.error("LA CAGASTE!", err));
  }

  render() {
    const {list} = this.state;
    return (
        <div className="container">
          <div className="row">
            {list.length > 0 &&
            list.map(({name, url}, index) => (
                <PokemonCard
                    key={name}
                    nombre={name}
                    url={url}
                    pokeNumber={index + 1}
                />
            ))}
          </div>
        </div>
    );
  }
}

export default PokemonList;
