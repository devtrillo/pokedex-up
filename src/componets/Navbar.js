import React from "react";

const Navbar = () => (
  <div className="navbar-fixed">
    <nav>
      <div className="nav-wrapper">
        <span className="brand-logo center">Pokedex</span>
      </div>
    </nav>
  </div>
);

/**
 * The navbar component Will show on top of the screen.
 * DOESN'T have a state
 * @returns {React.Component}
 */
export default Navbar;
