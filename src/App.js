import React from "react";
import Navbar from "./componets/Navbar";
import PokemonList from "./componets/pokemon/List";

const App = () => {
  return (
    <React.Fragment>
      <Navbar />
      <PokemonList />
    </React.Fragment>
  );
};

export default App;
